package repo

import (
	"context"
	"database/sql"
	"regexp"
	"strconv"
	"strings"

	"github.com/lib/pq"
)

const (
	DefaultLimit = 10
	MaxLimit     = 5000
)

func parse[T Cond](row interface {
	Scan(dest ...interface{}) error
}, out T) (err error) {
	arr := make([]any, 0, len(out.Cols(true, QInsert)))
	for _, v := range out.Cols(true, QInsert) {
		arr = append(arr, out.FieldByName(v))
	}
	if err = row.Scan(arr...); err != nil {
		err = pgError(err)
	}
	return
}

var re = regexp.MustCompile(`"(.*?)"`)

func quote(in string, extract bool) string {
	s := re.FindString(in)
	if len(s) == 0 {
		return strconv.Quote(in)
	}
	if extract {
		return s
	}
	return in
}

// Get get 1 entry. You must fill in at least 1 object field
// list allows you to set additional conditions for the search
func Get[T Cond](
	ctx context.Context,
	conn interface {
		QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
	},
	out T,
	list ...any,
) (err error) {
	var count = 0
	sqlArr := make([]string, 0, len(out.Cols(true, QSelect)))
	whereArr := make([]string, 0, len(out.Cols(true, QSelect)))
	arg := make([]any, 0, len(out.Cols(true, QSelect)))
	for _, v := range out.Cols(true, QSelect) {
		sqlArr = append(sqlArr, quote(v, false))
		if pt := out.ValueByName(v); pt != nil {
			count++
			whereArr = append(whereArr, quote(v, true)+`=$`+strconv.Itoa(count))
			arg = append(arg, pt)
		}
	}
	return parse(conn.QueryRowContext(ctx,
		`select `+strings.Join(sqlArr, ",")+` from `+out.Db()+
			(func() string { // where
				if count > 0 || len(list) > 0 {
					re := make([]string, 0, len(list)+1)
					if count > 0 {
						re = append(re, whereArr...)
					}
					st := ""
					for _, v := range list {
						switch v1 := v.(type) {
						case Operation:
							st += v1.String()
						case FieldNameToText:
							st += quote(string(v1), true) + "::text"
						case FieldName:
							st += quote(string(v1), true)
						case FieldValueArray:
							count++
							st += "ANY($" + strconv.Itoa(count) + ")"
							arg = append(arg, pq.Array(v1))
						case FieldValue:
							count++
							st += "$" + strconv.Itoa(count)
							arg = append(arg, v1)
						default:
							continue
						}
					}
					if len(st) > 0 {
						re = append(re, "("+st+")")
					}
					return ` where ` + strings.Join(re, " and ")
				}
				return ``
			}()),
		arg...,
	),
		out,
	)
}

// Insert insert 1 entry to database
// You must fill in at least 1 object field
func Insert[T Cond](
	ctx context.Context,
	conn interface {
		QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
	},
	in T,
) (err error) {
	var count = 0
	sqlArr := make([]string, 0, len(in.Cols(true, QInsert)))
	arg := make([]any, 0, len(in.Cols(true, QInsert)))
	for _, v := range in.Cols(true, QInsert) {
		if pt := in.ValueByName(v); pt != nil {
			count++
			sqlArr = append(sqlArr, quote(v, true))
			arg = append(arg, pt)
		}
	}
	s2 := make([]string, 0, len(sqlArr))
	for i := range sqlArr {
		s2 = append(s2, "$"+strconv.Itoa(i+1))
	}
	sqlRet := make([]string, 0, len(in.Primaries()))
	argRet := make([]any, 0, len(in.Primaries()))
	for _, v := range in.Primaries() {
		sqlRet = append(sqlRet, quote(v, true))
		argRet = append(argRet, in.FieldByName(v))
	}
	if err = conn.QueryRowContext(ctx,
		`insert into `+in.Db()+`(`+strings.Join(sqlArr, ",")+`) values(`+strings.Join(s2, ",")+`) RETURNING `+strings.Join(sqlRet, ",")+``,
		arg...,
	).Scan(argRet...); err != nil {
		return
	}
	return
}

// Upset insert or update if exist 1 entry to database
// You must fill in at least 1 object field except for the fields for the primary key
func Upset[T Cond](
	ctx context.Context,
	conn interface {
		QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
	},
	in T,
) (err error) {
	var count = 0
	if len(in.Cols(false, QUpdate)) == 0 {
		return ErrNoRowAffected
	}
	sqlArr := make([]string, 0, len(in.Cols(true, QInsert)))
	arg := make([]any, 0, len(in.Cols(true, QInsert)))
	for _, v := range in.Cols(true, QInsert) {
		if pt := in.ValueByName(v); pt != nil {
			count++
			sqlArr = append(sqlArr, quote(v, true))
			arg = append(arg, pt)
		}
	}
	sqlUpArr := make([]string, 0, len(in.Cols(false, QUpdate)))
	for _, v := range in.Cols(false, QUpdate) {
		if pt := in.ValueByName(v); pt != nil {
			sqlUpArr = append(sqlUpArr, quote(v, true)+`= EXCLUDED.`+quote(v, true))
		}
	}
	if len(sqlArr) == 0 {
		return ErrNoRowAffected
	}
	conflict := make([]string, 0, len(in.Primaries()))
	for _, v := range in.Primaries() {
		conflict = append(conflict, quote(v, true))
	}
	s2 := make([]string, 0, len(sqlArr))
	for i := range sqlArr {
		s2 = append(s2, "$"+strconv.Itoa(i+1))
	}
	sqlRet := make([]string, 0, len(in.Primaries()))
	argRet := make([]any, 0, len(in.Primaries()))
	for _, v := range in.Primaries() {
		sqlRet = append(sqlRet, quote(v, true))
		argRet = append(argRet, in.FieldByName(v))
	}
	if err = conn.QueryRowContext(ctx,
		`insert into `+in.Db()+`(`+strings.Join(sqlArr, ",")+`) values(`+strings.Join(s2, ",")+`) 
		on conflict (`+strings.Join(conflict, ",")+`) do update set `+strings.Join(sqlUpArr, ",")+
			` returning `+strings.Join(sqlRet, ","),
		arg...,
	).Scan(argRet...); err != nil {
		return
	}
	return
}

// Update update of existing 1 entry to database
// You must fill in at least 1 object field except for the fields for the primary key
func Update[T Cond](
	ctx context.Context,
	conn interface {
		ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	},
	in T,
) (err error) {
	var count = 0
	if len(in.Cols(false, QUpdate)) == 0 {
		return ErrNoRowAffected
	}
	sqlArr := make([]string, 0, len(in.Cols(false, QUpdate)))
	whereArr := make([]string, 0, len(in.Primaries()))
	arg := make([]any, 0, len(in.Cols(true, QUpdate)))
	for _, v := range in.Cols(false, QUpdate) {
		if pt := in.ValueByName(v); pt != nil {
			count++
			sqlArr = append(sqlArr, quote(v, true)+`=$`+strconv.Itoa(count))
			arg = append(arg, pt)
		}
	}
	if len(sqlArr) == 0 {
		return ErrNoRowAffected
	}
	for _, v := range in.Primaries() {
		count++
		whereArr = append(whereArr, quote(v, true)+`=$`+strconv.Itoa(count))
		arg = append(arg, in.ValueByName(v))
	}
	rez, err := conn.ExecContext(ctx,
		`update `+in.Db()+` set `+strings.Join(sqlArr, ",")+(func() string {
			if count > 0 {
				return ` where ` + strings.Join(whereArr, " and ")
			}
			return ""
		}()),
		arg...,
	)
	if err != nil {
		return
	}
	n, err := rez.RowsAffected()
	if err != nil {
		return
	}
	if n == 0 {
		return ErrNoRowAffected
	}
	return
}

// Delete delete of existing 1 entry to database
// You must fill all object fields for the primary key
func Delete[T Cond](
	ctx context.Context,
	conn interface {
		ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	},
	in T,
) (err error) {
	whereArr := make([]string, 0, len(in.Primaries()))
	arg := make([]any, 0, len(in.Primaries()))
	count := 0
	for _, v := range in.Primaries() {
		count++
		whereArr = append(whereArr, quote(v, true)+`=$`+strconv.Itoa(count))
		arg = append(arg, in.ValueByName(v))
	}
	rez, err := conn.ExecContext(ctx,
		`delete from `+in.Db()+(func() string {
			if count > 0 {
				return ` where ` + strings.Join(whereArr, " and ")
			}
			return ""
		}()),
		arg...,
	)
	if err != nil {
		return
	}
	n, err := rez.RowsAffected()
	if err != nil {
		return
	}
	if n == 0 {
		return ErrNoRowAffected
	}
	return
}

// List getting a paginated list
//
// cursor pointer to the first or last element on the page
// list allows you to set additional conditions for the search
func List[T Cond](
	ctx context.Context,
	conn interface {
		QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)
		QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
	},
	cursor T,
	req Request,
	fieldsOrder []string,
	list ...any,
) (out ListRez[T], err error) {
	const addNum = 2
	var (
		count  = 0
		maxLen = 0
	)
	sqlArr := make([]string, 0, len(cursor.Cols(true, QSelect)))
	arg := make([]any, 0, len(cursor.Cols(true, QSelect)))
	if req.Limit() > 0 && req.Limit() < MaxLimit {
		maxLen = req.Limit() + addNum
	} else {
		maxLen = DefaultLimit + addNum
	}
	for _, v := range cursor.Cols(true, QSelect) {
		sqlArr = append(sqlArr, quote(v, false))
	}
	isBackSort, qOrder, sign := GetOrder(req.Before(), req.Order())
	if len(fieldsOrder) == 0 {
		fieldsOrder = cursor.Primaries()
	}
	whereForCursor := ""
	if len(cursor.Cursor()) != 0 {
		if err = Get(ctx, conn, cursor); err != nil {
			return
		}
		whereArg := make([]string, 0, len(fieldsOrder))
		whereArr := make([]string, 0, len(fieldsOrder))
		for _, v := range fieldsOrder {
			count++
			whereArr = append(whereArr, strconv.Quote(v))
			whereArg = append(whereArg, "$"+strconv.Itoa(count))
			arg = append(arg, cursor.ValueByName(v))
		}
		if len(whereArg) > 0 {
			whereForCursor = strings.Join([]string{
				`(`,
				strings.Join(whereArr, ","),
				`)`,
				sign.Sign(),
				`(`,
				strings.Join(whereArg, ","),
				`)`,
			}, " ")
		}
	}
	rows, err := conn.QueryContext(ctx,
		strings.Join([]string{`select`, strings.Join(sqlArr, ","), `from`, cursor.Db(),
			func() string { // where
				if len(whereForCursor) > 0 || len(list) > 0 {
					re := make([]string, 0, len(list)+1)
					if len(whereForCursor) > 0 {
						re = append(re, whereForCursor)
					}
					st := ""
					for _, v := range list {
						switch v1 := v.(type) {
						case Operation:
							st += v1.String()
						case FieldName:
							st += quote(string(v1), true)
						case FieldNameToText:
							st += quote(string(v1), true) + "::text"
						case FieldValueArray:
							count++
							st += "ANY($" + strconv.Itoa(count) + ")"
							arg = append(arg, pq.Array(v1))
						case query:
							st += regexp.MustCompile(`\$(\d+)`).
								ReplaceAllStringFunc(
									v1.string,
									func(in string) string {
										i, err1 := strconv.Atoi(in[1:])
										if err1 != nil {
											panic(err)
										}
										if i == 0 || i > len(v1.arg) {
											panic(in + "not in arg")
										}
										if _, ok := v1.arg[i-1].(FieldValueArray); ok {
											count++
											arg = append(arg, pq.Array(v1.arg[i-1]))
										} else {
											count++
											arg = append(arg, v1.arg[i-1])
										}
										return "$" + strconv.Itoa(count)
									},
								)
						case FieldValue:
							count++
							st += "$" + strconv.Itoa(count)
							arg = append(arg, v1)
						}
					}
					if len(st) > 0 {
						re = append(re, "("+st+")")
					}
					return ` where ` + strings.Join(re, " and ")
				}
				return ``
			}(),
			func() string { // orderby
				orderByArr := make([]string, 0, len(fieldsOrder))
				for _, v := range fieldsOrder {
					orderByArr = append(orderByArr, quote(v, true))
				}
				if len(orderByArr) > 0 {
					return `order by (` + strings.Join(orderByArr, ",") + `) ` + qOrder.String()
				}
				return ""
			}(),
			func() string { // limit
				count++
				arg = append(arg, maxLen)
				return `limit $` + strconv.Itoa(count)
			}(),
		},
			" ",
		),
		arg...,
	)
	if err != nil {
		return
	}
	defer func() {
		_ = rows.Close()
	}()
	arr := make([]T, maxLen+1)
	isCursor := false
	count = 0
	for rows.Next() {
		val := cursor.New().(T)
		if err = parse(rows, val); err != nil {
			return
		}
		if val.Equal(cursor) {
			isCursor = true
			continue
		}
		if isBackSort {
			arr[maxLen-count-1] = val
		} else {
			arr[count] = val
		}
		count++
	}
	if count == 0 {
		err = ErrNoRows
		return
	} else {
		if isBackSort {
			arr = arr[maxLen-count : maxLen]
		} else {
			arr = arr[:count]
		}
	}
	return NewList(arr, isCursor, maxLen-addNum, req.Before()), nil
}

// Copy save array of element to database
func Copy[T Cond](
	ctx context.Context,
	conn interface {
		PrepareContext(ctx context.Context, query string) (*sql.Stmt, error)
		QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
	},
	arr []T,
) (err error) {
	if len(arr) == 0 {
		return ErrNoRowAffected
	} else if len(arr) == 1 {
		return Insert(ctx, conn, arr[0])
	}
	sqlArr := make([]string, 0, len(arr[0].Cols(true, QInsert)))
	for _, v := range arr[0].Cols(true, QInsert) {
		sqlArr = append(sqlArr, quote(v, true))
	}
	st, err := conn.PrepareContext(ctx, `copy `+arr[0].Db()+`(`+strings.Join(sqlArr, ",")+`) from stdin`)
	if err != nil {
		return
	}
	defer func() {
		if err == nil {
			if err = st.Close(); err != nil {
				err = pgError(err)
				return
			}
		} else {
			err = pgError(err)
		}
	}()
	var (
		arg = make([]any, len(arr[0].Cols(true, QInsert)))
	)
	for _, in := range arr {
		for i, v := range arr[0].Cols(true, QInsert) {
			arg[i] = in.ValueByName(v)
		}
		if _, err = st.ExecContext(ctx, arg...); err != nil {
			return
		}
	}
	return
}
