package repo

import (
	"reflect"
	"testing"
)

func Test_Query(t *testing.T) {
	type fields struct {
		fq     string
		q      string
		lim    int
		order  Direction
		before bool
	}
	type args struct {
		in []FieldName
	}
	tests := []struct {
		name   string
		args   args
		want   []any
		fields fields
	}{
		{
			name: "the fq and the q  are empty",
			args: args{
				in: []FieldName{},
			},
			want:   nil,
			fields: fields{},
		},
		{
			name: "the fq and the q  are empty. The FieldName array is set.",
			args: args{
				in: []FieldName{
					FieldName("dfdsfsdf"),
				},
			},
			want:   nil,
			fields: fields{},
		},
		{
			name: "the fq is set, but isn`t in the FieldName array. The q is empty",
			args: args{
				in: []FieldName{
					"20",
				},
			},
			want: nil,
			fields: fields{
				fq: "10",
			},
		},
		{
			name: "the fq is set, but isn`t in the FieldName array. The q is set",
			args: args{
				in: []FieldName{
					"20",
				},
			},
			want: nil,
			fields: fields{
				fq: "10",
				q:  "30",
			},
		},
		{
			name: "the fq is set. The FieldName array has fq. The q is set",
			args: args{
				in: []FieldName{
					"10",
				},
			},
			want: []any{FieldNameToText("10"), EQA, FieldValue("30")},
			fields: fields{
				fq: "10",
				q:  "30",
			},
		},
		{
			name: "the fq is set. The FieldName array has fq. The q is set",
			args: args{
				in: []FieldName{
					"10",
					"20",
				},
			},
			want: []any{FieldNameToText("20"), EQA, FieldValue("30")},
			fields: fields{
				fq: "20",
				q:  "30",
			},
		},
		{
			name: "the fq isn`t set. The q is set 1",
			args: args{
				in: []FieldName{
					"10",
				},
			},
			want: []any{FieldNameToText("10"), EQA, FieldValue("30")},
			fields: fields{
				q: "30",
			},
		},
		{
			name: "the fq isn`t set. The q is set 2",
			args: args{
				in: []FieldName{
					"10",
					"20",
					"30",
				},
			},
			want: []any{
				FieldNameToText("10"), EQA, FieldValue("30"),
				OR,
				FieldNameToText("20"), EQA, FieldValue("30"),
				OR,
				FieldNameToText("30"), EQA, FieldValue("30"),
			},
			fields: fields{
				q: "30",
			},
		},
		{
			name: "the fq isn`t set. The q is set 2",
			args: args{
				in: []FieldName{
					"10",
				},
			},
			want: []any{
				SB,
				FieldNameToText("10"), EQA, FieldValue("30"),
				AND,
				FieldNameToText("10"), EQA, FieldValue("20"),
				AND,
				FieldNameToText("10"), EQA, FieldValue("10"),
				EB,
			},
			fields: fields{
				q: "30 20 10",
			},
		},
		{
			name: "the fq isn`t set. The q is set 2",
			args: args{
				in: []FieldName{
					"10",
					"20",
					"30",
				},
			},
			want: []any{
				SB,
				FieldNameToText("10"), EQA, FieldValue("30"),
				AND,
				FieldNameToText("10"), EQA, FieldValue("20"),
				AND,
				FieldNameToText("10"), EQA, FieldValue("10"),
				EB,
				OR,
				SB,
				FieldNameToText("20"), EQA, FieldValue("30"),
				AND,
				FieldNameToText("20"), EQA, FieldValue("20"),
				AND,
				FieldNameToText("20"), EQA, FieldValue("10"),
				EB,
				OR,
				SB,
				FieldNameToText("30"), EQA, FieldValue("30"),
				AND,
				FieldNameToText("30"), EQA, FieldValue("20"),
				AND,
				FieldNameToText("30"), EQA, FieldValue("10"),
				EB,
			},
			fields: fields{
				q: "30 20 10",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := lim{
				fq:     tt.fields.fq,
				q:      tt.fields.q,
				lim:    tt.fields.lim,
				order:  tt.fields.order,
				before: tt.fields.before,
			}
			if got := l.Query(tt.args.in); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("lim.Query() = %v, want %v", got, tt.want)
			}
		})
	}
}
