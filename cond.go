package repo

// QType database query type
type QType int8

const (
	QSelect QType = iota
	QInsert
	QUpdate
)

type Cond interface {
	// FieldByName returns the address of the field of the structure given the name of the constant
	FieldByName(string) any
	// ValueByName returns the value of the field of the structure given the name of the constant
	ValueByName(string) any
	// New Create an object of type T
	New() any
	// Cursor returns a cursor of object
	//
	// Used for paginated list
	Cursor() []byte
	// Equal returns true if the objects are identical
	Equal(any) bool
	// Cols returns a list of table fields
	// primary - add primary keys to list
	// queryType - database query type
	Cols(primary bool, queryType QType) []string
	// Primaries returns a list of primary keys
	Primaries() []string
	// Db returns a name of database
	Db() string
}
