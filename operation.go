package repo

// allows you to build additional query conditions.
// The option is experimental and not fully thought out.
// This part will change.

type Operation int

const (
	END Operation = iota
	// AND
	AND
	// NOT
	NOT
	// OR
	OR
	// =
	EQ
	// <=
	LE
	// >=
	GE
	// <
	L
	// >
	G
	// (
	SB
	// )
	EB
	// ~*
	EQA
)

func (o Operation) String() string {
	switch o {
	default:
		return " "
	case AND:
		return " AND "
	case NOT:
		return " NOT "
	case OR:
		return " OR "
	case EQ:
		return " = "
	case LE:
		return " <= "
	case GE:
		return " >= "
	case L:
		return " < "
	case G:
		return " > "
	case SB:
		return " ( "
	case EB:
		return " ) "
	case EQA:
		return " ~* "
	}
}

type FieldName string

type FieldNameToText string

type FieldValue any

type FieldValueArray []any

type query struct {
	string
	arg []any
}

func (q *query) Arg(arg ...any) {
	q.arg = arg
}

func Query(q string, arg ...any) query {
	return query{
		string: q,
		arg:    arg,
	}
}

func ToFieldValueArray[
	E any,
	T interface {
		[]E
	},
](in T) FieldValueArray {
	ret := make(FieldValueArray, 0, len(in))
	for _, v := range in {
		ret = append(ret, v)
	}
	return ret
}
