package repo

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/lib/pq"
)

var ErrNoRows = errors.New("not found")
var ErrNoRowAffected = errors.New("no row affected")
var ErrDuplicateKeyValueViolates = errors.New("duplicate key value violates")
var ErrorUnknown = errors.New("unknown error")

func pgError(in error) error {
	if in == nil {
		return nil
	}
	if in == sql.ErrNoRows {
		return ErrNoRows
	}
	if pgErr, ok := in.(*pq.Error); ok {
		switch {

		}
		return fmt.Errorf("pg: constraint:%v code:%v column:%v table:%v schema:%v data_type_name:%v file:%v routine:%v detail:%v message:%v err: %w",
			pgErr.Constraint,
			pgErr.Code,
			pgErr.Column,
			pgErr.Table,
			pgErr.Schema,
			pgErr.DataTypeName,
			pgErr.File,
			pgErr.Routine,
			pgErr.Detail,
			pgErr.Message,
			ErrorUnknown,
		)
	}
	return in
}
