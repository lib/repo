package repo

// Direction sort order
type Direction int8

const (
	ASC Direction = iota
	DESC
)

func (d Direction) String() string {
	if d == DESC {
		return `DESC`
	}
	return `ASC`
}

type Sign int8

const (
	gte Sign = iota
	lte
)

func (i Sign) Sign() string {
	if i == gte {
		return `>=`
	}
	return `<=`
}

// GetOrder returns sort order and sign
func GetOrder(before bool, order Direction) (isBackSort bool, qOrder Direction, sign Sign) {
	if order == DESC {
		if before {
			sign = gte
			qOrder = ASC
			isBackSort = true
		} else {
			sign = lte
			qOrder = DESC
		}
	} else {
		if before {
			sign = lte
			qOrder = DESC
			isBackSort = true
		} else {
			sign = gte
			qOrder = ASC
		}
	}
	return
}
