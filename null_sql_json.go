package repo

type NullJson struct {
	Handler func(value []byte, ok bool)
}

func (ns *NullJson) Scan(value any) error {
	v, ok := value.([]byte)
	ns.Handler(v, ok && len(v) > 3)
	return nil
}
