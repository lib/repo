package repo

import (
	"errors"
	"strings"
)

type Request interface {
	// Limit number of elements per page
	Limit() int
	// Order sort order ASC or DESC
	Order() Direction
	// Before direction of scroll, default forward or backward
	Before() bool
	// used for fq, q
	Query([]FieldName) []any
}

func DefaultRequest() Request {
	return &lim{
		lim: DefaultLimit,
	}
}

func NewRequest(limit int, order Direction, before bool, fq, q string) Request {
	return &lim{
		lim:    limit,
		order:  order,
		before: before,
		fq:     fq,
		q:      q,
	}
}

func (l lim) Limit() int {
	return l.lim
}

func (l lim) Order() Direction {
	return l.order
}

func (l lim) Before() bool {
	return l.before
}

func (l lim) Query(in []FieldName) []any {
	if len(l.fq) > 0 && len(l.q) > 0 {
		for _, v := range in {
			if v == FieldName(l.fq) {
				return []any{
					FieldNameToText(v),
					EQA,
					FieldValue(l.q),
				}
			}
		}
	} else if len(l.q) > 0 {
		vls := strings.Split(l.q, " ")
		arg := []any{}
		for i, n := range in {
			if i > 0 {
				arg = append(arg, OR)
			}
			if len(vls) > 1 {
				arg = append(arg, SB)
				for j, v := range vls {
					if j > 0 {
						arg = append(arg, AND)
					}
					arg = append(arg, FieldNameToText(n), EQA, FieldValue(v))
				}
				arg = append(arg, EB)
			} else {
				arg = append(arg, FieldNameToText(n), EQA, FieldValue(l.q))
			}
		}
		return arg
	}
	return nil
}

type ListRez[T Cond] interface {
	Set([]T)
	Get() []T
	Start() []byte
	Stop() []byte
	After() bool
	Before() bool
}

func NewList[T Cond](in []T, isCursor bool, lim int, before bool) ListRez[T] {
	l := new(lst[T])
	l.isCursor = isCursor
	l.lim = lim
	l.before = before
	l.Set(in)
	return l
}

type lim struct {
	fq     string
	q      string
	lim    int
	order  Direction
	before bool
}

type lst[T Cond] struct {
	v                                   []T
	start, stop                         []byte
	lim                                 int
	isCursor, before, isAfter, isBefore bool
}

func (l lst[T]) Get() []T {
	return l.v
}
func (l *lst[T]) Set(in []T) {
	l.v = in
	l.calc()
}
func (l *lst[T]) calc() {
	ll := len(l.v)
	if ll == 0 {
		panic(errors.New("expect slice of values more than 0 len"))
	}
	if !l.isCursor {
		if ll > l.lim {
			if l.before {
				l.isBefore = true
				l.v = l.v[ll-l.lim : ll]
			} else {
				l.isAfter = true
				l.v = l.v[0:l.lim]
			}
		}
	} else {
		if l.before {
			l.isAfter = true
		} else {
			l.isBefore = true
		}
		if ll > l.lim {
			if l.before {
				l.isBefore = true
				l.v = l.v[ll-l.lim : ll]
			} else {
				l.isAfter = true
				l.v = l.v[0:l.lim]
			}
		}
	}
	if l.isBefore {
		l.start = (l.v[0]).Cursor()
	}
	if l.isAfter {
		l.stop = (l.v[len(l.v)-1]).Cursor()
	}
}

func (l lst[T]) Start() []byte {
	return l.start
}

func (l lst[T]) Stop() []byte {
	return l.stop
}

func (l lst[T]) After() bool {
	return l.isAfter
}

func (l lst[T]) Before() bool {
	return l.isBefore
}
